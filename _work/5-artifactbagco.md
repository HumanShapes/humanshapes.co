---
layout: work
style: "light"
title:  "Artifact Bag Co."
headline: "American Handmade"
image: "/images/artifact.jpg"
hero: "/images/artifact-hero.jpg"
tags: [design, development, shopify]
brand_color: "#E8EAEE"
grid_size: "large"
---

Artifact was founded in late 2010 by weirdo/genius Chris Hughes in Omaha, Nebraska. They make beautiful, minimal bags (and aprons) using traditional methods on vintage machines. Their work has been featured in [The New York Times][nyt], [Rolling Stone][rs], [GQ][gq], [Esquire][esquire], and [A Continuous Lean][acl] [(to name a few)][press].

![Artifact Website Images](/images/artifact-2.jpg)

### Expansion

In 2012, Artifact was about to expand its product offerings (and team). We helped transition them away from a random-ish Shopify theme (that served them well) to a custom theme that could handle many more product lines and grow with them.

The new website showcases the variety in material and style in Chris’ designs.

![Artifact Website Images](/images/artifact-1.jpg)

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

[site]: http://artifactbags.com "ArtifactBags.com"
[nyt]: http://www.nytimes.com/2011/01/23/fashion/23Noticed.html?_r=2&ref=fashion "NYTimes.com"
[rs]: http://blog.artifactbags.com/press/artifact-bag-co-in-rolling-stone-magazine "Rolling Stone"
[gq]: http://www.gq.com/style/blogs/the-gq-eye/2012/07/cool-stuff-artifact-lunch-tote.html?mbid=social_twitter_gqfashion "GQ.com"
[esquire]: http://blog.artifactbags.com/press/artifact-bag-co-in-esquire-magazine "Esquire.com"
[acl]: http://www.acontinuouslean.com/2013/12/23/making-america-artifact-bag/ "AContinuousLean.com"
[press]: http://artifactbags.com/pages/about/ "Artifact Press"

---
layout: work
style: "light no-text"
title:  "NEJS Conf"
headline: "“Javascript In The Wild”"
image: "/images/nejs.jpg"
hero: "/images/nejs-hero.png"
tags: design
brand_color: "#E9CA20"
brand_color_alt: "#7586f3"
grid_size: "small"
---

Some of our friends are hosting a Javascript conference this summer and asked us to put together its visual identity. With the event's location, [Omaha's Henry Doorly Zoo][zoo], in mind; we created a magical setting where computers, coffee, snakes, and birds live together to help you write some Javascript. We threw in a few references to Nebraska as well.

![NEJS Conf Logo][logo]

Development by [Zach Leatherman][zach] and the [NEJS][nejs] team.

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

[site]: http://nejsconf.com "NEJSconf.com"
[zoo]: https://en.wikipedia.org/wiki/Henry_Doorly_Zoo_and_Aquarium "View Wikipedia Entry"
[nejs]: http://nebraskajs.com/ "Nebraska JS"
[zach]: http://www.zachleat.com/web/ "Zach Leatherman"

[logo]: /images/nejs-1.png  "NEJS Conf Logo"

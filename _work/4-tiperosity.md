---
layout: work
style: "light"
title:  "Tiperosity"
headline: "Everyday Tips"
image: "/images/tiperosity-hero.jpg"
hero: "/images/tiperosity-hero.jpg"
tags: [development]
brand_color: "#003447"
brand_color_alt: ""
grid_size: "large"
category: "featured"
---

When we were kids, every time we had an idea, we'd say, "When I get older, I'm going to make [something ridiculous]." Well... we're older, and when we got the offer to help someone with their idea, we jumped at the opportunity.

![Tiperosity Homepage][home]

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

###Small Team

Nathan Preheim, co-founder of [MindMixer](http://mindmixer.com/about/), along with Justin Kemerling of [Justin Kemerling Design Co](http://www.justinkemerling.com/), looked to us for our web expertise. Soon after, we added Patrick and Marie Garmoe to the team to wrangle the content and lead our marketing efforts.

###Our Role

Human Shapes is in charge of development for Tiperosity. This includes the web app, marketing automation tools, and anything else.

We work closely with Justin Kemerling and the rest of the Tiperosity team to shape the product’s user experience in hopes of creating a place that’s easy to use, inviting, and worthy of the community we hope to cultivate.

![Tiperosity Categories][categories]

###Big Ambitions

Together, we hope to push this project as far as we can take it. We are excited to get feedback from visitors and create the best experience we can for our users.

![Tiperosity Icons][icons]

[site]: http://tiperosity.com "Tiperosity.com"

[home]: /images/blank.png
{: data-src="/images/tiperosity-1.jpg" data-src-retina="/images/tiperosity-1_2x.jpg" }

[categories]: /images/blank.png
{: data-src="/images/tiperosity-2.jpg" data-src-retina="/images/tiperosity-2_2x.jpg" }

[icons]: /images/blank.png
{: data-src="/images/tiperosity-3.jpg" data-src-retina="/images/tiperosity-3_2x.jpg" }

---
layout: work
style: "light"
title:  "Iowa Pays The Price"
headline: "Campaign Finance Reform"
image: "/images/iowapays-hero.jpg"
hero: "/images/iowapays-hero.jpg"
tags: [design, development, direction]
brand_color: "#00CF7D"
grid_size: "small"
category: "featured"
---

With the passing of Citizens United, campaign finance has gotten out of control (not to say it was under control before then). We worked with a new group of Iowa-based Politicians and Activists to brand their project and help get the word&nbsp;out.

![Desktop Homepage][desktop]

![Sections][sections]

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

### A Team Effort

An [Action Backed][ab] collaboration. Brand design by Justin Kemerling. Iconography by Jake Welchert. Website design and development by Human&nbsp;Shapes.

[site]: http://iowapaystheprice.org "IowaPaysThePrice.org"
[ab]: http://humanshapes.co/work/action-backed "Action Backed"

[desktop]: /images/blank.png
{: data-src="/images/iowapays-1.jpg" data-src-retina="/images/iowapays-1_2x.jpg" }

[sections]: /images/blank.png
{: data-src="/images/iowapays-2.jpg" data-src-retina="/images/iowapays-2_2x.jpg" }

---
layout: "work"
style: "light"
title:  "Viirt"
headline: "Roof replacement made easy."
image: "/images/viirt.png"
hero: "/images/viirt-hero.jpg"
tags: [design, development, direction]
brand_color: "#01C0EA"
brand_color_alt: "#7586f3"
grid_size: "large"
category: "featured"
---

Viirt is a roofing company using technology to combat inefficiencies in an outdated industry. We’ve worked with them since almost the beginning, designing and building a suite of tools to estimate, manage, and complete roofing jobs.

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

![Viirt Homepage][home]

### Instant Estimator

The typical estimation process involves opening up the yellow book, finding the "Roofing" section, calling some people, working with them to schedule a time, waiting for them to show up, having them "sell" to you, waiting a few days, and finally... receiving a quote.

We worked with Viirt and the folks at Object Lateral to design and build a tool that allows homeowners to get a rough estimate (within 5% accuracy) in less than a minute.

![][estimator]

## Project Dashboard

Homeowner’s that use Viirt are provided with a dashboard to follow along with their project. The dashboard houses project photos, information, and conversation. This allows the homeowner to track progress, ask questions, and stay as involved (or uninvolved) as they’d like.

The messaging system for projects is inspired by SMS and chat applications rather than email or more formal communication systems. This allows homeowners to feel comfortable that they can quickly ask any question without having to make a more formal request. Projects move fast, so the communication needs to match that.

## Contractor Dashboard

Viirt uses local contractors to install the homeowners’ new roofs. We helped build a dashboard that allows contractors to manage their projects, calendar, and client communication. We also add little tools such as today’s forecast which helps them plan their day (and reminds them to dress appropriately).

## The Viirt Blog

When you’re trying to re-think an entire industry, there are a lot of things you need to communicate. We took a few days and built a little Wordpress blog for the Viirt marketing team to share their thoughts, goals, and ideas with the industry and homeowners. It’s simple, but it does the job.

[site]: http://viirt.com "Viirt.com"

[home]: /images/blank.png
{: data-src="/images/viirt-1.jpg" }

[estimator]: /images/blank.png
{: data-src="/images/viirt-2.jpg" data-src-retina="/images/viirt-2_2x.jpg" }

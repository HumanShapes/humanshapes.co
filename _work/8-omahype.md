---
layout: work
style: "light"
title:  "Omahype"
headline: "Art & Culture Event Calendar"
image: "/images/omahype-hero.jpg"
hero: "/images/omahype-hero.jpg"
tags: [design, development, direction]
brand_color: "#040404"
grid_size: "large"
---

In the last four years, Omahype has promoted over 8,500 events at 87 venues in the Omaha area, posted hundreds of editorial pieces (including interviews, critical reviews and photo essays), thrown five events, sponsored dozens of other events, and even created a cat calendar.

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

### How It Started

In 2010 our friend (and design hero) [Micah Max][micah] had done some design work for a music blog named Omahype that shut down before the work got used. Around that time, [some friends of ours][team] were wanting to start a music and arts calendar for Omaha. We suggested seeing if we could takeover the Omahype name and URL, use Micah's branding, and keep the project alive. Andrew and Ian, the creators of the original Omahype, were gracious enough to let us take over the name. The project launched in December 2010.

### The Next Seven

We wanted Omahype to be the quickest and easiest place to find out what's going on in the city over the next seven days and beyond. Viewers should be able to open the site, skim the events, find something to do, then close the site and go have fun. If we slow them down just so we can show them more ads (a common practice on the web), then they aren't out in the city and we haven't done our job.

![Desktop Month View][desktop]

![Event Page][event]

[micah]: http://micahmax.com "MicahMax.com"
[site]: http://omahype.com "Omahype.com"
[team]: http://omahype.com/about "The Omahype Team"

[desktop]: /images/omahype-1.jpg  "Desktop Month View"
[event]: /images/omahype-2.jpg  "Event Page"

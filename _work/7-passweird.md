---
layout: work
style: "dark no-text"
title:  "Passweird"
headline: "Passwords too gross to steal."
image: "/images/passweird.jpg"
hero: "/images/passweird-hero.jpg"
tags: [concept, development, ruby, sinatra]
brand_color: "#04F717"
grid_size: "small"
---

While working on a much larger personal project that wasn’t coming together, we took a break and decide to endulge one of our weirder ideas. Passweird is a password generator that generates gross passwords under the pretense that nobody would want to steal something so gross.

We enlisted our friend [Matt Carlson][matt] to handle the illustration while we built the (super simple) web app. After we finished it, we got a bit nervous about pushing it out to the world, so we just quietly launched it.

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

![Passweird Illustrations][bg]

## A month later
After a month of just letting it sit there (and being a bit ashamed of making something so strange), it organically found an audience. Over the next month it got nearly 100,000 views. No promotion, no nothing. Just word-of-mouth and random retweeting.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">A very... vibrant password generator for a change. For the bookmarks: <a href="http://t.co/DH8vcLy0Qf">http://t.co/DH8vcLy0Qf</a></p>&mdash; Smashing Magazine (@smashingmag) <a href="https://twitter.com/smashingmag/status/418307038242820096">January 1, 2014</a></blockquote>

[site]: http://passweird.com "passweird.com"
[matt]: http://plaidmtn.com "Matt Carlson's Website"

[bg]: /images/passweird-1.jpg  "Passweird Illustrations"

---
layout: work
style: "light"
title:  "Action Backed"
headline: "Our Sister Company"
image: "/images/actionbacked.jpg"
hero: "/images/actionbacked-hero.jpg"
tags: [development]
brand_color: "#1b1e23"
brand_color_alt: "#e84353"
grid_size: "small"
category: "featured"
---

[Action Backed](http://actionbacked.org/) is a partnership between [Justin Kemerling Design Co](http://www.justinkemerling.com/) and [Human Shapes](Site). Together, we design and build web experiences for causes we believe in. Since 2013, we’ve teamed up to provide our strategy, speed, and expertise to over [fourteen](http://actionbacked.org/work.html) causes, with more on the way.

<span class="button-align-center">[View Website][site]{: class="button-site_link"}</span>

### Recent Projects

![Western Values Project Homepage][wvp]

In spring 2015, we worked with [Western Values Project](http://westernvaluesproject.org) to update their brand, and design and build their new website.

![Straight Shot Homepage][ss]

In early 2015, we worked with [Straight Shot](http://straightshot.co/) to develop a new brand, and design and build their new website.

![GoVote Website][gv]

In Fall 2014, we worked with [#GoVote](http://govote.org/) to create a website to encourage people to *#GoVote*. Designers from all over submitted #GoVote and #Vota images for people to view and share.

![YP Summit Website][yp]

We also designed and built the 2015 [Greater Omaha Young Professionals Summit](http://omahaypsummit.org/) website.

[site]: http://actionbacked.org "ActionBacked.org"

[wvp]: /images/blank.png
{: data-src="/images/actionbacked-wvp.jpg" data-src-retina="/images/actionbacked-wvp_2x.jpg" }

[ss]: /images/blank.png
{: data-src="/images/actionbacked-straightshot.jpg" data-src-retina="/images/actionbacked-straightshot_2x.jpg" }

[gv]: /images/blank.png
{: data-src="/images/actionbacked-govote.jpg" data-src-retina="/images/actionbacked-govote_2x.jpg" }

[yp]: /images/blank.png
{: data-src="/images/actionbacked-ypsummit.jpg" data-src-retina="/images/actionbacked-ypsummit_2x.jpg" }

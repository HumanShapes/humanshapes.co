$(document).on "ready", ->
  HS.init()

$(window).load ->

HS =
  sharePopup: (href) ->
    w = 600
    h = 300
    left = (screen.width / 2) - (w / 2)
    top = (screen.height / 2) - (h / 2)
    shareWindow = window.open(
        href
        "HumanShapes"
        "location=1,status=1,scrollbars=1,
        width=" + w + ",height=" + h + ",top=" + top + ",left=" + left
    )
    return false

  navToggle: ->
    $(".js-toggle-nav").click ->
        $("body").toggleClass "show-nav"

  onLoad: ->
    HS.onResize()
    HS.navToggle()
    # Lazy Loading of Images
    layzr = new Layzr (
        container: null
        selector: "[data-src]"
        attr: "data-src"
        retinaAttr: "data-src-retina"
        bgAttr: "data-src-bg"
        hiddenAttr: "data-src-hidden"
        threshold: 0
        callback: ->
            @classList.add "is-loaded"
    )

  onResize: ->

  init: ->
    HS.onLoad()
    $(window).resize ->
        HS.onResize()

    $(".js-open-contact, .js-close-contact").click (event) ->
        event.preventDefault()
        $("body").toggleClass "show-contact"
        return

    $(".js-share-popup").click (event) ->
        event.preventDefault()
        HS.sharePopup $(this).attr "href"
        return

    $(".contact-form").validate
        invalidHandler: (event, validator) ->
            $("#headline").animatescroll
                scrollSpeed: 600
                easing: "easeInOutQuad"
            setTimeout (->
                $("body").toggleClass("show-contact")
                return
            ), 600
            setTimeout (->
                $("body").toggleClass("has-contact-submission")
                return
            ), 800
        errorPlacement: ->
            false

class HS.Controller

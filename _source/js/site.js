(function() {
  var HS;

  $(document).on("ready", function() {
    return HS.init();
  });

  $(window).load(function() {});

  HS = {
    sharePopup: function(href) {
      var h, left, shareWindow, top, w;
      w = 600;
      h = 300;
      left = (screen.width / 2) - (w / 2);
      top = (screen.height / 2) - (h / 2);
      shareWindow = window.open(href, "HumanShapes", "location=1,status=1,scrollbars=1,        width=" + w + ",height=" + h + ",top=" + top + ",left=" + left);
      return false;
    },
    navToggle: function() {
      return $(".js-toggle-nav").click(function() {
        return $("body").toggleClass("show-nav");
      });
    },
    onLoad: function() {
      var layzr;
      HS.onResize();
      HS.navToggle();
      return layzr = new Layzr({
        container: null,
        selector: "[data-src]",
        attr: "data-src",
        retinaAttr: "data-src-retina",
        bgAttr: "data-src-bg",
        hiddenAttr: "data-src-hidden",
        threshold: 0,
        callback: function() {
          return this.classList.add("is-loaded");
        }
      });
    },
    onResize: function() {},
    init: function() {
      HS.onLoad();
      $(window).resize(function() {
        return HS.onResize();
      });
      $(".js-open-contact, .js-close-contact").click(function(event) {
        event.preventDefault();
        $("body").toggleClass("show-contact");
      });
      $(".js-share-popup").click(function(event) {
        event.preventDefault();
        HS.sharePopup($(this).attr("href"));
      });
      return $(".contact-form").validate({
        invalidHandler: function(event, validator) {
          $("#headline").animatescroll({
            scrollSpeed: 600,
            easing: "easeInOutQuad"
          });
          setTimeout((function() {
            $("body").toggleClass("show-contact");
          }), 600);
          return setTimeout((function() {
            $("body").toggleClass("has-contact-submission");
          }), 800);
        },
        errorPlacement: function() {
          return false;
        }
      });
    }
  };

  HS.Controller = (function() {
    function Controller() {}

    return Controller;

  })();

}).call(this);

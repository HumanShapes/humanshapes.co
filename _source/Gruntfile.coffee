###global module:false###

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %>\n' + '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' + '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' + ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n'
    favicons:
      options:
        appleTouchBackgroundColor: '#fff'
        appleTouchPadding: 30
        coast: true
        icoBackgroundColor: 'transparent'
      icons:
        src: 'favicons/source.png'
        dest: '../images/favicons/'
    modernizr: dist:
      'devFile': 'remote'
      'outputFile': '../js/modernizr-custom.js'
      'extra':
        'shiv': true
        'printshiv': false
        'load': true
        'mq': false
        'cssclasses': true
      'extensibility':
        'addtest': false
        'prefixed': false
        'teststyles': false
        'testprops': false
        'testallprops': false
        'hasevents': false
        'prefixes': false
        'domprefixes': false
      'uglify': true
      'tests': [
        'forms_placeholder'
        'rgba'
        'backgroundsize'
      ]
      'parseFiles': true
      'matchCommunityTests': false
      'customTests': []
    svgmin: files:
      expand: true
      cwd: 'svgs'
      src: [ '*.svg' ]
      dest: 'svgs/minified/'
    grunticon: icons:
      files: [ {
        expand: true
        cwd: 'svgs/minified/'
        src: [
          '*.svg'
          '*.png'
        ]
        dest: '../icons/'
      } ]
      options:
        datasvgcss: 'icons.data.svg.css'
        datapngcss: 'icons.data.png.css'
        urlpngcss: 'icons.fallback.css'
    coffee: compile: files: 'js/site.js': 'js/site.js.coffee'
    concat:
      options: separator: ';'
      dist:
        src: [
          'bower_components/jquery/dist/jquery.js'
          'bower_components/respond/dest/respond.src.js'
          'bower_components/animatescroll/animatescroll.js'
          'bower_components/jquery-validation/dist/jquery.validate.js'
          'bower_components/layzrjs/dist/layzr.js'
          'js/site.js'
        ]
        dest: '../js/compiled.js'
    watch:
      js:
        options: interrupt: true
        files: [ 'js/*.coffee' ]
        tasks: [ 'js' ]
      grunt: files: [ 'Gruntfile.js' ]
    copy:
      misc: files: [ {
        expand: true
        src: [ 'icons/**/*' ]
        dest: '../'
      } ]
      favicon: files: [ {
        expand: true
        cwd: 'favicons/'
        src: [ 'favicon.ico' ]
        dest: '../images/favicons/'
      } ]
    uglify: build: files: '../js/compiled.min.js': '../js/compiled.js'
    imagemin: dynamic: files: [ {
      expand: true
      cwd: 'images/'
      src: [ '*.{png,jpg,gif}' ]
      dest: '../images/'
    } ]
    grunt.loadNpmTasks 'grunt-favicons'
    grunt.loadNpmTasks 'grunt-contrib-watch'
    grunt.loadNpmTasks 'grunt-contrib-copy'
    grunt.loadNpmTasks 'grunt-contrib-uglify'
    grunt.loadNpmTasks 'grunt-contrib-coffee'
    grunt.loadNpmTasks 'grunt-contrib-concat'
    grunt.loadNpmTasks 'grunt-contrib-imagemin'
    grunt.loadNpmTasks 'grunt-svgmin'
    grunt.loadNpmTasks 'grunt-grunticon'
    grunt.loadNpmTasks 'grunt-modernizr'
    grunt.registerTask 'js', [
      'coffee'
      'concat'
      'uglify'
    ]
    grunt.registerTask 'icons', [
      'svgmin'
      'grunticon'
      'copy:misc'
    ]
    grunt.registerTask 'all', [
      'favicons'
      'modernizr'
      'icons'
      'copy'
      'js'
      'imagemin'
    ]
    grunt.registerTask 'default', [
      'copy'
      'js'
    ]
    grunt.registerTask 'favicon', [
      'favicons'
      'copy:favicon'
    ]
  return

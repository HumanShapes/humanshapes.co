---
layout: post
date: 2015-6-29 12:00
title: Mindmixer Is Now mySidewalk
tags: [client, MindMixer, mySidewalk]
author: jason

---

We’ve worked with the folks over at MindMixer many times throughout the years. We helped them build the last two versions of their website, and prototype the first responsive version of their app. We love what they are trying to do and believe in them fully.

After over five years, they are changing the name of the company and taking it in a slightly different, and super exciting, new direction.

We’re excited for what’s next.

Learn more about the transition on [this micro-site][1] we helped them with (designed by their amazing team).

*Update: The mySidewalk site has since been updated.*

[1]:	https://rebrand.mysidewalk.com/ "View Site"

---
layout: post
date: 2015-7-20 12:00
title: Artifact Prototype
tags: [client, Artifact Bag Co., design, inspiration]
author: cody

---

Check out this beautiful prototype of a new bag from our awesome client, [Artifact Bag Co.](http://artifactbags.com) Chris and his team make such gorgeous work, and it just keeps getting better.

![Artifact][image-1]

I can’t wait to get ahold of one (or three) of these.

[image-1]:	/images/post-artifact-prototype.jpg "Aritfact Prototype"
